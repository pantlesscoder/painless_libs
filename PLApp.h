/*****************************************************************************
 FILE: PLApp - project: https://codeberg.org/pantlesscoder/painless_libs
  Cross-platform application creation library.
  To use this file, you should add this:
    #define PL_LIBS_IMPL
    or
    #define PL_APP_IMPL
  before you include this file in *one* of your C or C++ file to add the
  implementation, otherwise you only include the definitions of the pl_app API.
  E.g:
    #include ...
    #include ...
    #include ...
    #define PL_APP_IMPL
    #include "PLApp.h"

 AUTHOR: Pantless Coder (Nghia Lam)
 LICENSE: MIT License - Copyright (c) 2022 Nghia Lam (Pantless Coder)
  Please see the LICENSE file for more details.

 ----
 TODO(Nghia Lam): List of features that need to be done as MVP.
  - [ ] Support Windows, MacOS, Linux native API. (Other platform will be
    re-consider after finish this task).
  - [ ] Support DirectX, OpenGL (Vulkan will be an optional).
  - [ ] Windows handler & manager.
  - [ ] Input handler & manager.
  - [ ] Audio support.
*****************************************************************************/

#ifndef PL_APP_H
#define PL_APP_H

#define PL_APP_VERSION "0.0.1"

#if !defined(_WIN32) && (defined(__WIN32__) || defined(WIN32) || defined(__MINGW32__))
    #define _WIN32
#endif /* _WIN32 */

#if defined(PL_LIBS_IMPL) && !defined(PL_APP_IMPL)
    #define PL_APP_IMPL
#endif
#if defined(PL_LIBS_DLL) && !defined(PL_APP_DLL)
    #define PL_APP_DLL
#endif

#ifndef PLAPI
    #if defined(_WIN32) && defined(PL_LOG_DLL) && defined(PL_LOG_IMPL)
        #define PLAPI __declspec(dllexport)
    #elif defined(_WIN32) && defined(PL_LOG_DLL)
        #define PLAPI __declspec(dllimport)
    #else
        #define PLAPI extern
    #endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 * Type & Structure definitions.
 *****************************************************************************/

/*****************************************************************************
 * Application and Window related APIs.
 *****************************************************************************/

PLAPI void PLAppInit(const char* Title, int Width, int Height);
PLAPI void PLAppCleanup();
PLAPI void PLAppIsRunning();

#ifdef __cplusplus
}
#endif

#endif /* PL_APP_H */

/*****************************************************************************
 * Implementation.
 *****************************************************************************/
#if defined(PL_APP_IMPL)

#ifndef PLAPI_IMPL
    #define PLAPI_IMPL
#endif

#endif /* PL_APP_IMPL */
