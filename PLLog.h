/*****************************************************************************
 FILE: PLLog - project: https://codeberg.org/pantlesscoder/painless_libs
  Simple logging library with speed and ease of use written in C.
  To use this file, you should add this:
    #define PL_LIBS_IMPL
    or
    #define PL_LOG_IMPL
  before you include this file in *one* of your C or C++ file to add the
  implementation, otherwise you only include the definitions of the pl_app API.
  E.g:
    #include ...
    #include ...
    #include ...
    #define PL_LOG_IMPL
    #include "PLLog.h"

 AUTHOR: Pantless Coder (Nghia Lam)
 LICENSE: MIT License - Copyright (c) 2022 Nghia Lam (Pantless Coder)
  Please see the LICENSE file for more details.
*****************************************************************************/

#ifndef PL_LOG_H
#define PL_LOG_H

#define PL_LOG_VERSION "0.0.1"

#if defined(PL_LIBS_IMPL) && !defined(PL_LOG_IMPL)
    #define PL_LOG_IMPL
#endif
#if defined(PL_LIBS_DLL) && !defined(PL_LOG_DLL)
    #define PL_LOG_DLL
#endif

#ifndef PLAPI
    #if defined(_WIN32) && defined(PL_LOG_DLL) && defined(PL_LOG_IMPL)
        #define PLAPI __declspec(dllexport)
    #elif defined(_WIN32) && defined(PL_LOG_DLL)
        #define PLAPI __declspec(dllimport)
    #else
        #define PLAPI extern
    #endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 * Type & structure definition.
 *****************************************************************************/

#define MAX_LOGLINE_LENGTH 155
#define MAX_LOGNAME_LENGTH 50

typedef enum
{
    ELogTrace = 0,
    ELogDebug = 1,
    ELogInfo  = 2,
    ELogWarn  = 3,
    ELogError = 4,
    ELogFatal = 5,
} ELogLevel;

typedef struct
{
    char Name[MAX_LOGNAME_LENGTH];
    ELogLevel Level;
} PLogger;

/*****************************************************************************
 * Logging APIs.
 *****************************************************************************/

#define PLLOG_TRACE(LoggerPtr, ...) PLLogProcess(LoggerPtr, ELogTrace, __FILE__, __LINE__, __VA_ARGS__)
#define PLLOG_DEBUG(LoggerPtr, ...) PLLogProcess(LoggerPtr, ELogDebug, __FILE__, __LINE__, __VA_ARGS__)
#define PLLOG_INFO(LoggerPtr, ...)  PLLogProcess(LoggerPtr, ELogInfo, __FILE__, __LINE__, __VA_ARGS__)
#define PLLOG_WARN(LoggerPtr, ...)  PLLogProcess(LoggerPtr, ELogWarn, __FILE__, __LINE__, __VA_ARGS__)
#define PLLOG_ERROR(LoggerPtr, ...) PLLogProcess(LoggerPtr, ELogError, __FILE__, __LINE__, __VA_ARGS__)
#define PLLOG_FATAL(LoggerPtr, ...) PLLogProcess(LoggerPtr, ELogFatal, __FILE__, __LINE__, __VA_ARGS__)

PLAPI void PLLogSetLoggerName(PLogger* Logger, const char* Name);
PLAPI void PLLogSetLoggerLevel(PLogger* Logger, ELogLevel Level);
PLAPI void PLLogProcess(PLogger* Logger, ELogLevel Level, const char* File, int Line, const char* Message, ...);

#ifdef __cplusplus
}
#endif

#endif /* PL_LOG_H */

/*****************************************************************************
 * Implementation.
 *****************************************************************************/
#if defined(PL_LOG_IMPL)

#include <stdio.h>  /* Required for: vprintf() */
#include <stdarg.h> /* Required for: va_list, va_start(), va_end() */
#include <stdlib.h> /* Required for: exit() */
#include <string.h> /* Required for: strcpy()  */

#ifndef PLAPI_IMPL
    #define PLAPI_IMPL
#endif

PLAPI void PLLogSetLoggerName(PLogger* Logger, const char* Name)
{
    strcpy(Logger->Name, Name);
}

PLAPI void PLLogSetLoggerLevel(PLogger* Logger, ELogLevel Level)
{
    Logger->Level = Level;
}

PLAPI_IMPL void PLLogProcess(PLogger* Logger, ELogLevel Level, const char* File, int Line, const char* Message, ...)
{
    if (Level < Logger->Level) return;

    // Init the output string.
    char Buffer[MAX_LOGLINE_LENGTH];
    char* Target = Buffer;

    switch (Level)
    {
        case ELogTrace:
            Target += sprintf(Target, "[%s] %s%-5s\x1b[0m ", Logger->Name, "\x1b[94m", "TRACE");
            break;
        case ELogDebug:
            Target += sprintf(Target, "[%s] %s%-5s\x1b[0m ", Logger->Name, "\x1b[36m", "DEBUG");
            break;
        case ELogInfo:
            Target += sprintf(Target, "[%s] %s%-5s\x1b[0m ", Logger->Name, "\x1b[32m", "INFO");
            break;
        case ELogWarn:
            Target += sprintf(Target, "[%s] %s%-5s\x1b[0m ", Logger->Name, "\x1b[33m", "WARN");
            break;
        case ELogError:
            Target += sprintf(Target, "[%s] %s%-5s\x1b[0m ", Logger->Name, "\x1b[31m", "ERROR");
            break;
        case ELogFatal:
            Target += sprintf(Target, "[%s] %s%-5s\x1b[0m ", Logger->Name, "\x1b[35m", "FATAL");
            break;
    }
    Target += sprintf(Target, "\x1b[90m%s:%d:\x1b[0m ", File, Line);

    va_list Args;
    va_start(Args, Message);
    vsnprintf(Target, MAX_LOGLINE_LENGTH, Message, Args);
    va_end(Args);

    puts(Buffer);
    fflush(stdout);

    if (Level == ELogFatal) exit(EXIT_FAILURE);
}

#endif /* PL_LOG_IMPL */
